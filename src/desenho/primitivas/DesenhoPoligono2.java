package desenho.primitivas;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import java.util.Random;

public class DesenhoPoligono2 implements GLEventListener {

    @Override
    public void display(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        //define a cor de limpeza do fundo em RGBA
        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
        //escolhe a cor do desenho
        gl.glColor3f(0.5f, 1, 1);
        //Começando pelo triangulo do telhado
        //começa a fazer a primitiva triangulo do telhado
        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex2f(37, 75);
        gl.glVertex2f(56, 112);
        gl.glVertex2f(75, 75);
        //termina o triangulo do telhado
        gl.glEnd();
        //Começando o paralelogramo do teto
        //começa a fazer a primitiva paralelogramo do teto
        gl.glBegin(GL2.GL_QUAD_STRIP);
        gl.glVertex2f(56, 112);
        gl.glVertex2f(75, 75);
        gl.glVertex2f(118, 137);
        gl.glVertex2f(137, 100);
        //termina o palalelogramo do teto
        gl.glEnd();
        //Começando a fazer a frente
        //começa a fazer a primitiva da frente
        gl.glBegin(GL.GL_LINE_STRIP);
        gl.glVertex2f(37, 75);
        gl.glVertex2f(37, 25);
        gl.glVertex2f(75, 25);
        gl.glVertex2f(75, 75);
        //termina a frente
        gl.glEnd();
        //Começando a fazer a parede
        //começa a fazer a primitiva da parede
        gl.glBegin(GL.GL_LINE_STRIP);
        gl.glVertex2f(75, 25);
        gl.glVertex2f(137, 50);
        gl.glVertex2f(137, 100);
        //termina a parede
        gl.glEnd();
        //Começando a fazer a porta
        //começa a fazer a primitiva da porta
        gl.glBegin(GL2.GL_QUADS);
        gl.glVertex2f(50, 50);
        gl.glVertex2f(50, 25);
        gl.glVertex2f(62, 25);
        gl.glVertex2f(62, 50);
        //termina a porta
        gl.glEnd();
        //Começando a fazer a chamine
        //começa a fazer a primitiva da chamine
        gl.glBegin(GL2.GL_QUADS);
        gl.glVertex2f(93, 50);
        gl.glVertex2f(93, 68);
        gl.glVertex2f(125, 81);
        gl.glVertex2f(125, 62);
        //termina a chamine
        gl.glEnd();
        //força a execução para a formaçao do desenho ate aqui.
        gl.glFlush();
    }

    @Override
    public void dispose(GLAutoDrawable arg0) {
        // TODO Auto-generated method stub 
    }

    @Override
    public void init(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glMatrixMode(GL2.GL_MATRIX_MODE);
        gl.glLoadIdentity();
        GLU glu = new GLU();
        glu.gluOrtho2D(0, 200f, 0f, 150f);
    }

    @Override
    public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
        // TODO Auto-generated method stub
    }

    public static void main(String[] args) {

        GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities caps = new GLCapabilities(profile);
        GLCanvas canvas = new GLCanvas(caps);
        canvas.addGLEventListener(new DesenhoPoligono2());
        //cria um frame
        JFrame frame = new JFrame("Desenho da casinha");
        frame.add(canvas);
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
