package desenho.primitivas;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import java.util.Random;

public class DesenhoPontos2 implements GLEventListener {

    @Override
    public void display(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        //define a cor de limpeza do fundo em RGBA
        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
        //escolhe a cor do desenho
        gl.glColor3f(0, 1f, 0);
        float i = 1;
        //faz um gerador randomico
        Random gerador = new Random();
        int X = 500;
        while (i <= X) {
            //escolhe a cor do ponto   
            gl.glColor3f(gerador.nextFloat(), gerador.nextFloat(), gerador.nextFloat());
            //altera o tamanho do ponto
            gl.glPointSize(gerador.nextInt(30));
            //começa a fazer a primitiva
            gl.glBegin(GL.GL_POINTS);
            //cria o vertice
            gl.glVertex2f(gerador.nextInt(200), gerador.nextInt(150));
            i = i + 1;
            //Termina de fazer a primitiva  
            gl.glEnd();
        }
        //força a execução para a formaçao do desenho ate aqui.
        gl.glFlush();
    }

    @Override
    public void dispose(GLAutoDrawable arg0) {
        // TODO Auto-generated method stub 
    }

    @Override
    public void init(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glMatrixMode(GL2.GL_MATRIX_MODE);
        gl.glLoadIdentity();
        GLU glu = new GLU();
        glu.gluOrtho2D(0, 200f, 0f, 150f);
    }

    @Override
    public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
        // TODO Auto-generated method stub
    }

    public static void main(String[] args) {

        GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities caps = new GLCapabilities(profile);
        GLCanvas canvas = new GLCanvas(caps);
        canvas.addGLEventListener(new DesenhoPontos2());
        //cria um frame
        JFrame frame = new JFrame("Desenho de pontos 2");
        frame.add(canvas);
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
