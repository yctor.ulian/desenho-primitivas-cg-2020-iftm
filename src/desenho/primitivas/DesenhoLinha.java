package desenho.primitivas;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import java.util.Random;

public class DesenhoLinha implements GLEventListener {

    @Override
    public void display(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        //define a cor de limpeza do fundo em RGBA
        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
        //escolhe a cor do desenho
        gl.glColor3f(0, 1f, 0);
        float i = 1;
        Random gerador = new Random();
        int X = 100;
        while (i <= X) {
            gl.glColor3f(gerador.nextFloat(), gerador.nextFloat(), gerador.nextFloat());
            gl.glLineWidth(25);
            //Habilita a utilizaçao de um recurso de linha na biblioteca
            gl.glEnable(GL2.GL_LINE_STIPPLE);
            gl.glLineStipple(gerador.nextInt(255), (short) gerador.nextInt(255));
            //começa a fazer a primitiva
            gl.glBegin(GL.GL_LINES);
            //criaçao de vertices
            gl.glVertex2f(0 + 10 * i, -25);
            gl.glVertex2f(0 + 10 * i, 149);
            //termina a primitiva    
            gl.glEnd();
            //desabilita a utilizaçao de um recurso de linha na biblioteca
            gl.glDisable(GL2.GL_LINE_STIPPLE);
            i = i + 1;
            //termina a primitiva      
            gl.glEnd();
        }
        //força a execução para a formaçao do desenho ate aqui.
        gl.glFlush();
    }

    @Override
    public void dispose(GLAutoDrawable arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void init(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glMatrixMode(GL2.GL_MATRIX_MODE);
        gl.glLoadIdentity();
        GLU glu = new GLU();
        glu.gluOrtho2D(0, 1000f, 0f, 150f);
    }

    @Override
    public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
        // TODO Auto-generated method stub
    }

    public static void main(String[] args) {

        GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities caps = new GLCapabilities(profile);
        GLCanvas canvas = new GLCanvas(caps);
        canvas.addGLEventListener(new DesenhoLinha());
        //cria um frame
        JFrame frame = new JFrame("Desenho de linha");
        frame.add(canvas);
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
